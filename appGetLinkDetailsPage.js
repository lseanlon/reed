var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');

var jsonListIndexStr = "[" + fs.readFileSync("./dist/listingIndex.txt", 'utf8') + "{}]";
var jsonIndexList = JSON.parse(jsonListIndexStr);

var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};


//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;

//get config
//loop config, 
// loop indexes

var listOfLink = {}

var downloadedEntryPathList = [];
//download all the first page listing
jsonIndexList.forEach(function(_elem, index, collection) {
 
    var randomNum = Math.floor(Math.random() * 574) + 1; 
  
    if (!_elem || !_elem.numberOfPage) {
        return;
    }

    for (var i = 1; i <= _elem.numberOfPage; i++) {
        addAllLinkInformation(i, _elem, timerWait + randomNum, i == _elem.numberOfPage);

    }

});




function addAllLinkInformation(i, _elem, timerWait, _isLastIndex) {
    // grab file, cheerio
    // grab link html

    // setTimeout(function() { 

    var fileContent = fs.readFileSync(_elem.filePathFormat + i + ".txt", 'utf8');
    var $ = cheerio.load(fileContent)


    if (!listOfLink[_elem.ref]) { listOfLink[_elem.ref] = []; }
    var temList = [];

    $('h3.title').each(function(i, element) {

        var curLink = $(this).find('a').prop('href');
        var curRow = { "link": 'http://www.reed.co.uk'+ curLink, "ref": _elem.ref ? _elem.ref : "" }; 
      
         console.log(" curRow ", curRow);
        listOfLink[_elem.ref].push(curRow);
    });




    _isLastIndex = true;
    if (_isLastIndex) {
        fs.writeFileSync("./dist/detailLinkIndex.txt", JSON.stringify(listOfLink));
    }
    console.log('written to file  /dist/detailLinkIndex.txt ');

    // }, i * 3000);
}
