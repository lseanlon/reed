var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');

var resultKeywordList = [];

var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/configuration/UrlList.txt';
    process.argv.forEach(function(val, index, array) {

        // node app  src/UrlList.txt 
        // eg. [0] node / [1] app/ [2]'src/UrlListInfo.txt'
        //console.log(index + ': ' + val);  

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};
//read main file for reference
// read recordFinalDetails reference file
// parse json
// loop entire folder, read entire index file
// download google result for each keyword

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;


var chunks = require('chunk-array').chunks
var finalResultList = [];
var finalResultExcelList = [];
var downloadedEntryPathList = [];

require('shelljs/global');

// Sync call to exec()
var version = exec('node --version', { silent: true }).output;


//download all the first page listing
urlList.forEach(function(_elem, index, collection) {
    var urlInfo = _elem.split("|");
    var ref = urlInfo[2];

    var jsonFileDetails = fs.readFileSync('./dist/downloaded-' + ref + 'SearchData.txt', 'utf8');
    jsonFileDetails = JSON.parse(jsonFileDetails);

    var listRef = jsonFileDetails;
    for (var i = 0; i < listRef.length; i++) {
        if (listRef[i]) {
            //console.log(listRef[i]);
            readPage(i, 1, listRef[i], timerWait, (i + 1) == listRef.length);


        }


    }



});



function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url = urlparts[0] + '?' + pars.join('&');
        return url;
    } else {
        return url;
    }
}

function readPage(i, j, _elem, timerWait, _isLastIndex) {
    console.log('readPage j =', j);


    var keyword = _elem["keyword" + j];


    if (!_elem || j > 3) {
        return;
    }
    if (!_elem['jobId']) {
        _elem['jobId'] = "i" + i
    }
    console.log('readPage keyword ', keyword);
    var filepath = _elem['outputfilepath-keyword' + j];
    var fileContent = '';
    try {

        fileContent = fs.readFileSync(filepath, 'utf8');
        console.log('success to read ', filepath);
    } catch (e) {
        console.log('failed to read ', filepath);
    }

    var $ = cheerio.load(fileContent);
    var sanitizeDash = function(_val) {
        //take out all html   
        return ($("<p>" + _val + "</p>").text());
    }


    $elemList = $('h3 a[href]');
    var linkList = [];
    if (!resultKeywordList) { resultKeywordList = []; }
    $elemList.each(function(i, element) {
        var link = $(element).prop('href')
        link = "http://google.com" + link;
        var temRow = { 'originalLink': _elem['originalLink'], 'title': _elem['title'], 'company': sanitizeDash(_elem['company']), 'keyword': keyword, 'link': link }

        if (i) {
            temRow['keyword'] = '';
            temRow['originalLink'] = '';
            temRow['title'] = '';
            temRow['company'] = '';
        } else {
            temRow['zz_contactName'] = _elem['contactName'];
            temRow['zz_jobId'] = _elem['jobId'];
            temRow['zz_rawJobDesc'] = _elem['rawJobDesc'];
            temRow['zz_jobDesc'] = _elem['jobDesc'];
            temRow['zz_experienceRequirements'] = _elem['experienceRequirements'];
            temRow['zz_test'] = _elem['test'];
            temRow['zz_area'] = _elem['area'];
            temRow['zz_salary'] = _elem['salary'];
            temRow['zz_company'] = _elem['company'];
            temRow['zz_employmentType'] = _elem['employmentType'];
            temRow['zz_title'] = _elem['title'];
            temRow['zz_companyUrl'] = _elem['companyUrl'];
            temRow['zz_ref'] = _elem['ref'];
            temRow['zz_isDownloaded'] = _elem['isDownloaded'];
            temRow['zz_outputfilepath-keyword1'] = _elem['outputfilepath-keyword1'];
            temRow['zz_outputfilepath-keyword2'] = _elem['outputfilepath-keyword2'];
            temRow['zz_outputfilepath-keyword3'] = _elem['outputfilepath-keyword3'];
        }
        resultKeywordList.push(temRow);
        linkList.push(link);

    });




    _elem['searchResultKeyword' + j] = linkList.join("\n\n");


    //remove old similar - prevent same id pushed twice
    var newList = [];
    for (var i = 0; i < finalResultList.length; i++) {
        if (finalResultList[i].jobId != _elem.jobId) {
            newList.push(finalResultList[i]);
        }

    }
    finalResultList = newList;
    finalResultList.push(_elem);

    if (!finalResultExcelList[_elem.ref]) { finalResultExcelList[_elem.ref] = []; }

    var newList = [];
    for (var i = 0; i < finalResultExcelList[_elem.ref].length; i++) {
        if (finalResultExcelList[_elem.ref][i].jobId != _elem.jobId) {
            newList.push(finalResultExcelList[_elem.ref][i]);
        }

    }
    finalResultExcelList[_elem.ref] = newList;
    finalResultExcelList[_elem.ref].push(_elem);
    _isLastIndex = true;

    if (_isLastIndex && _elem.ref) {}
    var pathname = "./dist/FinalResultWithSearch-" + _elem.ref + ".txt"
    console.log("Write download result to path ", pathname);
    fs.writeFileSync(pathname, JSON.stringify(finalResultList));

    //write into excel 
    var json2xls = require('json2xls');
    for (var key in finalResultExcelList) {
        if (finalResultExcelList.hasOwnProperty(key)) {
            // console.log(key + " -> " + finalResultExcelList[key]);  
            var xls = json2xls(finalResultExcelList[key]);
            var outexcelpath = './dist/FinalResultWithSearch' + key + '-data.xlsx';
            console.log("Write to excel ", outexcelpath);
            fs.writeFileSync(outexcelpath, xls, 'binary');
        }
    }


    // console.log(key + " -> " + resultKeywordList[key]);  
    var xls = json2xls(resultKeywordList);
    var outexcelpath = './dist/FinalResultWithSearchLink' + key + '-data.xlsx';
    console.log("Write to excel ", outexcelpath);
    fs.writeFileSync(outexcelpath, xls, 'binary');

    //repeat for different index
    if (j < 3) { readPage(i, j + 1, _elem, timerWait, _isLastIndex); }

    try {

        delete _elem['jobType'];
        delete _elem['contactName'];
        delete _elem['jobId'];
        delete _elem['rawJobDesc'];
        delete _elem['jobDesc'];
        delete _elem['experienceRequirements'];
        delete _elem['test'];
        delete _elem['area'];
        delete _elem['salary'];
        delete _elem['employmentType'];
        delete _elem['title'];
        delete _elem['companyUrl'];
        delete _elem['ref'];
        delete _elem['isDownloaded'];
        delete _elem['outputfilepath-keyword1'];
        delete _elem['outputfilepath-keyword2'];
        delete _elem['outputfilepath-keyword3'];
    } catch (e) {
        console.log(e);
    }


}
