var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
var mkdirp = require('mkdirp');
require('shelljs/global');

var jsonListIndexStr = fs.readFileSync("./dist/detailLinkIndex.txt", 'utf8');
var jsonIndexList = JSON.parse(jsonListIndexStr);

var getUrlEntry = function() {
    var paramValue = './src/configuration/UrlList.txt';
    process.argv.forEach(function(val, index, array) {

        // node app  src/UrlList.txt 
        // eg. [0] node / [1] app/ [2]'src/UrlListInfo.txt'
        //console.log(index + ': ' + val);  

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");
var outputJson = {};


//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;


var listOfLink = {};
//download all the listing links
urlList.forEach(function(_elem, index, collection) {
    var urlInfo = _elem.split("|");
    var urlPath = urlInfo[0];
    var urlLink = urlInfo[1];
    var ref = urlInfo[2];


    var randomNum = Math.floor(Math.random() * 574) + 1;
    var list = jsonIndexList[ref];
    for (var i = 1; i <= list.length; i++) {

        downloadAllLinkPage(i, list[i], timerWait + randomNum, (i + 1) == list.length)
    }


});

function exitHandler(options, err) {
    if (options.cleanup) console.log('clean');
    if (err) console.log(err.stack);
    if (options.exit) process.exit();
}

function downloadAllLinkPage(i, _elem, timerWait, _isLastIndex) {
    var extraTimer = 0;

    if (!_elem) {
        return;
    }

    var urlLinkPage = _elem.link;
    var outputfolderpath = "./dist/FinalPage/" + _elem.ref + "/";

    mkdirp(outputfolderpath, function(err) {
        if (err) return cb(err);
        console.log("create folder path", outputfolderpath);
    });

    var outputfilepath = outputfolderpath + i + ".txt";

    if (fs.existsSync(outputfilepath)) {
        extraTimer = 0;
    } else {
        extraTimer = 2650;
    }

    setTimeout(function() {


        if (fs.existsSync(outputfilepath)) {
            console.log("File downloaded. Already found. Skipped downloading ", urlLinkPage);
            console.log("File downloaded. Skipped downloading path ", outputfilepath);

        } else {


            console.log("start downloading ", urlLinkPage);
            // download(urlLinkPage, outputfolderpath).then((data) => {
            //     fs.writeFileSync(outputfilepath, data);
            //     console.log('done download - ' + urlLinkPage + ' into ' + outputfilepath);
            // }); 


            var commandRun = 'phantomjs PageDownload6.js "' + (urlLinkPage) + '"';
            try {

                exec(commandRun, function(status, output) {
                    console.log('Exit status:', status);
                    console.log('Program output:');

                    if (!urlLinkPage || !output) {
                        return;
                    }

                    fs.writeFileSync(outputfilepath, output);
                    console.log('done download - ' + urlLinkPage + ' into ' + outputfilepath);

                });

            } catch (e) {
                console.log(e);

            }

        }

        var curRow = { 'urlLinkPage': urlLinkPage, 'outputfilepath': outputfilepath };

        if (!listOfLink[_elem.ref]) {
            listOfLink[_elem.ref] = [];
        }
        listOfLink[_elem.ref].push(curRow);
        _isLastIndex = true;
        if (_isLastIndex) {
            var pathname = "./dist/downloadedFinalDetails-" + _elem.ref + ".txt"
            console.log("Write download result to path ", pathname);
            fs.writeFileSync(pathname, JSON.stringify(listOfLink));
        }

    }, (i * extraTimer));

}
